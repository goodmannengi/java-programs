import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {
		
		
		System.out.println("Enter string to be reversed");
		Scanner scan = new Scanner(System.in);
		String read = scan.nextLine();

		StringBuilder input = new StringBuilder(read);

		System.out.println("Reversed string is:");
		System.out.println(input.reverse().toString());

	}

	
}
import java.util.Scanner;
public class Approximate{

	public static void roundDown(int num){
		System.out.println("Enter decimal value");
		Scanner input = new Scanner(System.in);
		double number = input.nextDouble();
		double newValue = Math.floor(number);
		System.out.println((int)newValue);
	}

	public static void roundUp(int num){
		System.out.println("Enter decimal value");
		Scanner input = new Scanner(System.in);
		double number = input.nextDouble();
		double newValue = Math.ceil(number);
		System.out.println((int)newValue);
	}

	public static void main(String[] args) {
		int value = 0;
		roundDown(value);
		roundUp(value);
	}
}
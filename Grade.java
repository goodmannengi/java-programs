import javax.swing.JOptionPane;
public class Grade{
	public static void checker(int input){
		String grade  = JOptionPane.showInputDialog("Enter your grade.");
		int gradee = Integer.parseInt(grade);

		if (gradee > 69) {
			JOptionPane.showMessageDialog(null, "You have an A. Congratulations!");
		}
		else if (gradee >59 && gradee<70) {
			JOptionPane.showMessageDialog(null, "You have a B. Congratulations!");
		}
		else if (gradee >49 && gradee<60) {
			JOptionPane.showMessageDialog(null, "You have a C. Well done!");
		}
		else if (gradee >39 && gradee<50) {
			JOptionPane.showMessageDialog(null, "You have a D. You could do better!");
		}
		else if (gradee >29 && gradee<40) {
			JOptionPane.showMessageDialog(null, "You have an E. A little more effort would make a lot of difference!");
		}
		else if(gradee <30) {
			JOptionPane.showMessageDialog(null, "You have an F. Failure isnt final!");
		}
	}
	public static void main(String[] args) {
		int num = 0;
		checker(num);
		
	}
}